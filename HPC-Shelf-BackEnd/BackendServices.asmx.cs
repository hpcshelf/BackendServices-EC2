﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Web.Services;
using Amazon;
using Amazon.EC2;
using Amazon.EC2.Model;
using org.hpcshelf.backend.ec2;

namespace org.hpcshelf.backend
{
    public interface IBackendServices
    {
        string deploy(string platform_config);
        string release(string platform_address);
    }

    public class BackendServices : WebService, IBackendServices
    {
//        protected static List<IVirtualMachine> virtual_machines = new List<IVirtualMachine>();
//        public static List<IVirtualMachine> VirtualMachines { get { return virtual_machines; } }

        private string EC2_VIRTUALPLATFORM_IMAGE_ID { get { return File.ReadAllLines("EC2_VIRTUALPLATFORM_IMAGE_ID")[0]; } }

        private RegionEndpoint DEFAULT_REGION = RegionEndpoint.USEast1; //RegionEndpoint.USEast1=Virginia // RegionEndpoint.SAEast1=Sao Paulo

        private static string ScriptRunException = "Script RunException!!!";

        private static string default_instance_type = InstanceType.T2Micro.ToString();

        private IDictionary<Tuple<string, string, string>, string> instance_type_dict = new Dictionary<Tuple<string, string, string>, string>()
                    {
                        { new Tuple<string, string, string>("general", "A1", "Medium"), "a1.medium" },
                        { new Tuple<string, string, string>("general", "A1", "Large"), "a1.large" },
                        { new Tuple<string, string, string>("general", "A1", "Large1x"), "a1.xlarge" },
                        { new Tuple<string, string, string>("general", "A1", "Large2x"), "a1.2xlarge" },
                        { new Tuple<string, string, string>("general", "A1", "Large4x"), "a1.4xlarge" },
                        { new Tuple<string, string, string>("general", "A1", "Metal"), "a1.metal" },
                        { new Tuple<string, string, string>("general", "T3", "Nano"), "t3.nano" },
                        { new Tuple<string, string, string>("general", "T3", "Micro"), "t3.micro" },
                        { new Tuple<string, string, string>("general", "T3", "Small"), "t3.small" },
                        { new Tuple<string, string, string>("general", "T3", "Medium"), "t3.medium" },
                        { new Tuple<string, string, string>("general", "T3", "Large"), "t3.large" },
                        { new Tuple<string, string, string>("general", "T3", "Large1x"), "t3.xlarge" },
                        { new Tuple<string, string, string>("general", "T3", "Large2x"), "t3.2xlarge" },
                        { new Tuple<string, string, string>("general", "T3a", "Nano"), "t3a.nano" },
                        { new Tuple<string, string, string>("general", "T3a", "Micro"), "t3a.micro" },
                        { new Tuple<string, string, string>("general", "T3a", "Small"), "t3a.small" },
                        { new Tuple<string, string, string>("general", "T3a", "Medium"), "t3a.medium" },
                        { new Tuple<string, string, string>("general", "T3a", "Large"), "t3a.large" },
                        { new Tuple<string, string, string>("general", "T3a", "Large1x"), "t3a.xlarge" },
                        { new Tuple<string, string, string>("general", "T3a", "Large2x"), "t3a.2xlarge" },
                        { new Tuple<string, string, string>("general", "T2", "Nano"), "t2.nano" },
                        { new Tuple<string, string, string>("general", "T2", "Micro"), "t2.micro" },
                        { new Tuple<string, string, string>("general", "T2", "Small"), "t2.small" },
                        { new Tuple<string, string, string>("general", "T2", "Medium"), "t2.medium" },
                        { new Tuple<string, string, string>("general", "T2", "Large"), "t2.large" },
                        { new Tuple<string, string, string>("general", "T2", "Large1x"), "t2.xlarge" },
                        { new Tuple<string, string, string>("general", "T2", "Large2x"), "t2.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large"), "m5.large" },
                        { new Tuple<string, string, string>("general", "M5", "Large1x"), "m5.xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large2x"), "m5.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large4x"), "m5.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large8x"), "m5.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large12x"), "m5.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large16x"), "m5.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Large24x"), "m5.24xlarge" },
                        { new Tuple<string, string, string>("general", "M5", "Metal"), "m5.metal" },
                        { new Tuple<string, string, string>("general", "M5d", "Large"), "m5d.large" },
                        { new Tuple<string, string, string>("general", "M5d", "Large1x"), "m5d.xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large2x"), "m5d.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large4x"), "m5d.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large8x"), "m5d.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large12x"), "m5d.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large16x"), "m5d.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Large24x"), "m5d.24xlarge" },
                        { new Tuple<string, string, string>("general", "M5d", "Metal"), "m5d.metal" },
                        { new Tuple<string, string, string>("general", "M5a", "Large"), "m5d.large" },
                        { new Tuple<string, string, string>("general", "M5a", "Large1x"), "m5a.xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large2x"), "m5a.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large4x"), "m5a.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large8x"), "m5a.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large12x"), "m5a.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large16x"), "m5a.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5a", "Large24x"), "m5a.24xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large"), "m5ad.large" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large1x"), "m5ad.xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large2x"), "m5ad.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large4x"), "m5ad.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large8x"), "m5ad.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large12x"), "m5ad.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large16x"), "m5ad.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5ad", "Large24x"), "m5ad.24xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large"), "m5n.large" },
                        { new Tuple<string, string, string>("general", "M5n", "Large1x"), "m5n.xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large2x"), "m5n.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large4x"), "m5n.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large8x"), "m5n.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large12x"), "m5n.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large16x"), "m5n.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5n", "Large24x"), "m5n.24xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large"), "m5dn.large" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large1x"), "m5dn.xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large2x"), "m5dn.2xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large4x"), "m5dn.4xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large8x"), "m5dn.8xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large12x"), "m5dn.12xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large16x"), "m5dn.16xlarge" },
                        { new Tuple<string, string, string>("general", "M5dn", "Large24x"), "m5dn.24xlarge" },
                        { new Tuple<string, string, string>("general", "M4", "Large"), "m4.large" },
                        { new Tuple<string, string, string>("general", "M4", "Large1x"), "m4.xlarge" },
                        { new Tuple<string, string, string>("general", "M4", "Large2x"), "m4.2xlarge" },
                        { new Tuple<string, string, string>("general", "M4", "Large4x"), "m4.4xlarge" },
                        { new Tuple<string, string, string>("general", "M4", "Large10x"), "m4.10xlarge" },
                        { new Tuple<string, string, string>("general", "M4", "Large16x"), "m4.16xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large"), "c5.large" },
                        { new Tuple<string, string, string>("compute", "C5", "Large1x"), "c5.xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large2x"), "c5.2xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large4x"), "c5.4xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large9x"), "c5.9xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large12x"), "c5.12xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large18x"), "c5.18xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Large24x"), "c5.24xlarge" },
                        { new Tuple<string, string, string>("compute", "C5", "Metal"), "c5.metal" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large"), "c5d.large" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large1x"), "c5d.xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large2x"), "c5d.2xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large4x"), "c5d.4xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large9x"), "c5d.9xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large12x"), "c5d.12xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large18x"), "c5d.18xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Large24x"), "c5d.24xlarge" },
                        { new Tuple<string, string, string>("compute", "C5d", "Metal"), "c5d.metal" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large"), "c5n.large" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large1x"), "c5n.xlarge" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large2x"), "c5n.2xlarge" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large4x"), "c5n.4xlarge" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large9x"), "c5n.9xlarge" },
                        { new Tuple<string, string, string>("compute", "C5n", "Large18x"), "c5n.18xlarge" },
                        { new Tuple<string, string, string>("compute", "C5n", "Metal"), "c5n.metal" },
                        { new Tuple<string, string, string>("compute", "C4", "Large"), "c4.large" },
                        { new Tuple<string, string, string>("compute", "C4", "Large1x"), "c4.xlarge" },
                        { new Tuple<string, string, string>("compute", "C4", "Large2x"), "c4.2xlarge" },
                        { new Tuple<string, string, string>("compute", "C4", "Large4x"), "c4.4xlarge" },
                        { new Tuple<string, string, string>("compute", "C4", "Large8x"), "c4.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large"), "r5.large" },
                        { new Tuple<string, string, string>("memory", "R5", "Large1x"), "r5.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large2x"), "r5.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large4x"), "r5.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large8x"), "r5.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large12x"), "r5.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large16x"), "r5.16xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Large24x"), "r5.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R5", "Metal"), "r5.metal" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large"), "r5d.large" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large1x"), "r5d.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large2x"), "r5d.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large4x"), "r5d.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large8x"), "r5d.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large12x"), "r5d.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large16x"), "r5d.16xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Large24x"), "r5d.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R5d", "Metal"), "r5d.metal" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large"), "r5a.large" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large1x"), "r5a.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large2x"), "r5a.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large4x"), "r5a.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large8x"), "r5a.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large12x"), "r5a.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large16x"), "r5a.16xlarge" },
                        { new Tuple<string, string, string>("memory", "R5a", "Large24x"), "r5a.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large"), "r5ad.large" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large1x"), "r5ad.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large2x"), "r5ad.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large4x"), "r5ad.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large12x"), "r5ad.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5ad", "Large24x"), "r5ad.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large"), "r5n.large" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large1x"), "r5n.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large2x"), "r5n.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large4x"), "r5n.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large8x"), "r5n.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large12x"), "r5n.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large16x"), "r5n.16xlarge" },
                        { new Tuple<string, string, string>("memory", "R5n", "Large24x"), "r5n.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large"), "r5dn.large" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large1x"), "r5dn.xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large2x"), "r5dn.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large4x"), "r5dn.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large8x"), "r5dn.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large12x"), "r5dn.12xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large16x"), "r5dn.16xlarge" },
                        { new Tuple<string, string, string>("memory", "R5dn", "Large24x"), "r5dn.24xlarge" },
                        { new Tuple<string, string, string>("memory", "R4", "Large"), "r4.large" },
                        { new Tuple<string, string, string>("memory", "R4", "Large1x"), "r4.xlarge" },
                        { new Tuple<string, string, string>("memory", "R4", "Large2x"), "r4.2xlarge" },
                        { new Tuple<string, string, string>("memory", "R4", "Large4x"), "r4.4xlarge" },
                        { new Tuple<string, string, string>("memory", "R4", "Large8x"), "r4.8xlarge" },
                        { new Tuple<string, string, string>("memory", "R4", "Large16x"), "r4.16xlarge" },
                        { new Tuple<string, string, string>("memory", "X1e", "Large2x"), "x1e.2xlarge" },
                        { new Tuple<string, string, string>("memory", "X1e", "Large4x"), "x1e.4xlarge" },
                        { new Tuple<string, string, string>("memory", "X1e", "Large8x"), "x1e.8xlarge" },
                        { new Tuple<string, string, string>("memory", "X1e", "Large16x"), "x1e.16xlarge" },
                        { new Tuple<string, string, string>("memory", "X1e", "Large32x"), "x1e.32xlarge" },
                        { new Tuple<string, string, string>("memory", "X1", "Large16x"), "x1.16xlarge" },
                        { new Tuple<string, string, string>("memory", "X1", "Large32x"), "x1.32xlarge" },
//                        { new Tuple<string, string, string>("memory", "HighMemory", "Metal_u_6tb1"), "x1e." },
//                        { new Tuple<string, string, string>("memory", "HighMemory", "Metal_u_9tb1"), "." },
//                        { new Tuple<string, string, string>("memory", "HighMemory", "Metal_u_12tb1"), "." },
//                        { new Tuple<string, string, string>("memory", "HighMemory", "Metal_u_18tb1"), "." },
//                        { new Tuple<string, string, string>("memory", "HighMemory", "Metal_u_24tb1"), "." },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large"), "z1d.large" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large1x"), "z1d.xlarge" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large2x"), "z1d.2xlarge" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large3x"), "z1d.3xlarge" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large6x"), "z1d.6xlarge" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Large12x"), "z1d.12xlarge" },
                        { new Tuple<string, string, string>("memory", "Z1d", "Metal"), "z1d.metal" },
                        { new Tuple<string, string, string>("accelerated", "P3", "Large2x"), "p3.2xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P3", "Large8x"), "p3.8xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P3", "Large16x"), "p3.16xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P3", "Large24x"), "p3.24xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P2", "Large1x"), "p2.xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P2", "Large8x"), "p2.8xlarge" },
                        { new Tuple<string, string, string>("accelerated", "P2", "Large16x"), "p2.16xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large1x"), "g4dn.xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large2x"), "g4dn.2xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large4x"), "g4dn.4xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large8x"), "g4dn.8xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large16x"), "g4dn.16xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Large12x"), "g4dn.12xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G4dn", "Metal"), "g4dn.metal" },
                        { new Tuple<string, string, string>("accelerated", "G3", "Large1x"), "g3.xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G3", "Large4x"), "g3.4xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G3", "Large8x"), "g3.8xlarge" },
                        { new Tuple<string, string, string>("accelerated", "G3", "Large16x"), "g3.16xlarge" },
                        { new Tuple<string, string, string>("accelerated", "F1", "Large2x"), "f1.2xlarge" },
                        { new Tuple<string, string, string>("accelerated", "F1", "Large4x"), "f1.4xlarge" },
                        { new Tuple<string, string, string>("accelerated", "F1", "Large16x"), "f1.16xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Large"), "i3.large" },
                        { new Tuple<string, string, string>("storage", "I3", "Large1x"), "i3.xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Large2x"), "i3.2xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Large4x"), "i3.4xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Large8x"), "i3.8xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Large16x"), "i3.16xlarge" },
                        { new Tuple<string, string, string>("storage", "I3", "Metal"), "i3.metal" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large"), "i3en.large" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large1x"), "i3en.xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large2x"), "i3en.2xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large3x"), "i3en.3xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large6x"), "i3en.6xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large12x"), "i3en.12xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Large24x"), "i3en.24xlarge" },
                        { new Tuple<string, string, string>("storage", "I3en", "Metal"), "i3en.metal" },
                        { new Tuple<string, string, string>("storage", "D2", "Large1x"), "d2.xlarge" },
                        { new Tuple<string, string, string>("storage", "D2", "Large2x"), "d2.2xlarge" },
                        { new Tuple<string, string, string>("storage", "D2", "Large4x"), "d2.4xlarge" },
                        { new Tuple<string, string, string>("storage", "D2", "Large8x"), "d2.8xlarge" },
                        { new Tuple<string, string, string>("storage", "H1", "Large2x"), "h1.2xlarge" },
                        { new Tuple<string, string, string>("storage", "H1", "Large4x"), "h1.4xlarge" },
                        { new Tuple<string, string, string>("storage", "H1", "Large8x"), "h1.8xlarge" },
                        { new Tuple<string, string, string>("storage", "H1", "Large16x"), "h1.16xlarge" }
        };

        public BackendServices()
        {
            ServicePointManager.DefaultConnectionLimit = 1024;
        }

        private static int deploy_id_counter = 0;
        private static ConcurrentQueue<int> deploy_id_queue = new ConcurrentQueue<int>();
        //private static IDictionary<string, int> deploy_id_map = new Dictionary<string, int>();
        private static readonly object lock_deploy_id = new object();
        private string HPCShelf_HOME = Environment.GetEnvironmentVariable("HPCShelf_HOME");

        [WebMethod]
        public string deploy(string platform_config)
        {
            int deploy_id = default(int);
            try
            {
                if (!deploy_id_queue.TryDequeue(out deploy_id))
                    lock (lock_deploy_id)
                    {
                        deploy_id = deploy_id_counter++;
                    }

                Console.WriteLine("deploy id ... {0} ", deploy_id);

                IList<IVirtualMachine> VirtualMachines = new List<IVirtualMachine>();

                IList<string> ids = new List<string>();

                Console.WriteLine("STARTING DEPLOYING -- {0}", platform_config == null);
                Console.WriteLine("PLATFORM: {0}", platform_config);
                //  Console.WriteLine("mutex id: {0}", mutex.GetHashCode());

                string instance_type_root = null;
                string[] config;
                string[] lines = platform_config.Split('|');
                config = lines[0].Split(';');

                //string[] config = platform_config.Split(';');

                RegionEndpoint region = config.Length < 3 || config[2].Equals("*") ? DEFAULT_REGION : RegionEndpoint.GetBySystemName(config[2]);

                //   mutex.WaitOne();
                AmazonEC2Client client = new AmazonEC2Client(region);
                //    mutex.Release();

                VirtualMachines.Clear();

                string keyPairName = "hpc-shelf-credential";
                //     mutex.WaitOne();
                InstancesManager.CreateKeyPair(client, keyPairName, Path.Combine(Utils.CURRENT_FOLDER, keyPairName + ".pem"));
                //     mutex.Release();

                string securityGroupName = "hpc-shelf";

                string instanceId = "";
                string amID = config[0].Equals("*") ? EC2_VIRTUALPLATFORM_IMAGE_ID : config[0];
                int n = config.Length < 2 || config[1].Equals("*") ? 3 : int.Parse(config[1]) + 1; // number of nodes in the virtual platform ... TODO ...
                string instance_type = config.Length < 6 || config[3].Equals("*") ? default_instance_type : instance_type_dict[new Tuple<string, string, string>(config[3], config[4], config[5])];
                if (lines.Length > 1)
                {
                    string[] config_root = lines[1].Split(';');
                    instance_type_root = instance_type_dict[new Tuple<string, string, string>(config_root[0], config_root[1], config_root[2])];
                }
                else
                    instance_type_root = instance_type;

                Console.WriteLine("INSTANCE TYPE is {0} -- {1} -- lines.Length={2}", instance_type, instance_type_root == instance_type, lines.Length);
                Console.WriteLine("AMI is {0}", amID);
                Console.WriteLine("Region is {0}", region.DisplayName);

                //        mutex.WaitOne();
                instanceId = InstancesManager.LaunchInstance(instance_type_root, amID, keyPairName, securityGroupName, client);
                //         mutex.Release();

                ids.Add(instanceId);

                for (int i = 1; i < n; i++)
                {
                    //           mutex.WaitOne();
                    instanceId = InstancesManager.LaunchInstance(instance_type, amID, keyPairName, securityGroupName, client);
                    //          mutex.Release();
                    ids.Add(instanceId);
                }

                Thread.Sleep(1000);
                int current = 0;

                while (current < ids.Count)
                {
                    Console.Write("Status ");
                    string instance_id = ids.ElementAt(current);
                    string status_check = InstancesManager.Instance_statusCheck(region, instance_id);
                    Console.WriteLine(instance_id + ": " + status_check);
                    if (!status_check.Equals("ok"))
                    {
                        Console.WriteLine("Waiting to running: " + instance_id);
                        Thread.Sleep(5000);
                    }
                    else
                    {
                        current++;
                        DataRegistry(client, instance_id, VirtualMachines);
                    }
                }
                string platform_address = VirtualMachines.ElementAt(0).PublicIpAddress;
                string platform_address_local = VirtualMachines.ElementAt(0).PrivateIpAddress;

                generatePeerHosts(deploy_id, VirtualMachines);
                mpi(deploy_id, platform_address, platform_address_local, VirtualMachines);
                startShelf(deploy_id, platform_address, platform_address_local, VirtualMachines);

                if (VirtualMachines.Count > 0)
                {
                    int port_platform = 8080;
                    string platform_id = string.Format("http://{0}:{1}/PlatformServices.asmx", platform_address, port_platform);
                    // deploy_id_map[platform_id] = deploy_id;
                    running_instances[platform_id] = new Tuple<IList<string>, AmazonEC2Client>(ids, client);
                    return platform_id;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Config: " + e.Message);
                throw;
            }
            finally
            {
                deploy_id_queue.Enqueue(deploy_id);
            }

            return "";
        }

        private static IDictionary<string, Tuple<IList<string>, AmazonEC2Client>> running_instances = new ConcurrentDictionary<string, Tuple<IList<string>, AmazonEC2Client>>();

        public string status(RegionEndpoint region, IList<IVirtualMachine> VirtualMachines)
        {
            int current = 0;
            int size = 0;
            while (current < VirtualMachines.Count) 
            {
                Console.Write("Status ");
                string instance_id = VirtualMachines.ElementAt(current).InstanceId;
                string status = InstancesManager.Instance_statusCheck(region, instance_id);
                Console.WriteLine(instance_id + ": " + status);
                if (!status.Equals("ok")) 
                {
                    Console.WriteLine("waiting instance {0} : {1}", instance_id, status);
                    Thread.Sleep(100);
                } else size++;
                current++;
            }
            return (size == VirtualMachines.Count)? "OK" : "NO";
        }


      // private static Semaphore mutex =  new Semaphore(1,1);

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string mpi(int deploy_id, string platform_address_global, string platform_address_local, IList<IVirtualMachine> VirtualMachines)
        {
            if (VirtualMachines.Count > 0)
            {
                Console.WriteLine("mpi enter: {0}", deploy_id);
                try
                {
                    Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, string.Format("run1 {0}", deploy_id)));
                    Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, string.Format("run2 {0}", deploy_id)));
                    Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, string.Format("gac_config {0}", deploy_id)));
                }
                catch (Exception e)
                {
                    Console.WriteLine(ScriptRunException);
                }
                Console.WriteLine("mpi exit: {0}", deploy_id);

                return platform_address_global;
            }

            return "VirtualMachines List is empty!!";
        }

        //      private static readonly object lock_2 = new object();

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string startShelf(int deploy_id, string platform_address_global, string platform_address_local, IList<IVirtualMachine> VirtualMachines)
        {
            if (VirtualMachines.Count > 0)
            {
                Console.WriteLine("startShelf enter: {0}", deploy_id);

                try
                {
                    Thread thread = new Thread(() => xsp4_shelf_root_run(platform_address_global, platform_address_local));
                    thread.Start();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ScriptRunException);
                }
                Console.WriteLine("startShelf exit: {0}", deploy_id);
                return platform_address_global;
            }

            return "VirtualMachines List is empty!!";
        }


        private static void xsp4_shelf_root_run(string platform_address_global, string platform_address_local)
        {
            Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, "startShelf") + " " + platform_address_global + " " + platform_address_local + " &");
        }

        protected void DataRegistry(AmazonEC2Client client, string instanceId, IList<IVirtualMachine> VirtualMachines) 
        {
            //lock (lock_1)
            {
                DescribeInstancesRequest req = new DescribeInstancesRequest()
                {
                    Filters = new List<Filter>()
                {
                    new Filter()
                    {
                        Name = "instance-id",
                        Values = new List<String>() { instanceId }
                    }
                }
                };
                IVirtualMachine vm = null;
                while (vm == null || vm.PublicIpAddress == null)
                {
                    List<Reservation> result = client.DescribeInstances(req).Reservations;

                    foreach (Amazon.EC2.Model.Reservation reservation in result)
                    {
                        foreach (Instance runningInstance in reservation.Instances)
                        {
                            vm = new VirtualMachine
                            {
                                InstanceId = runningInstance.InstanceId,
                                PrivateIpAddress = runningInstance.PrivateIpAddress,
                                PublicIpAddress = runningInstance.PublicIpAddress
                            };
                        }
                    }
                    if (vm == null || vm.PublicIpAddress == null)
                        Thread.Sleep(1000); //Wait 1s for aws release public IP address
                }
                VirtualMachines.Add(vm);
            }
        }

        //  private static readonly object lock_3 = new object();

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void generatePeerHosts(int deploy_id, IList<IVirtualMachine> VirtualMachines)
        {
            if (VirtualMachines.Count > 0)
            {
                Console.WriteLine("generatePeerHosts enter: {0}", deploy_id);

                IEnumerator<IVirtualMachine> it = VirtualMachines.GetEnumerator();
                it.MoveNext();

                List<string> hosts = new List<string>();
                List<string> peers = new List<string>();
                List<string> workpeers = new List<string>();
                List<string> address = new List<string>();
                List<string> workers = new List<string>();
                List<string> workers_host_file = new List<string>();
                int count = 0;
                // int portWorkers = 4000;

                peers.Add("root");
                hosts.Add("127.0.0.1 localhost");
                hosts.Add(it.Current.PrivateIpAddress + " root");
                address.Add(it.Current.PublicIpAddress);
                workers.Add("-n 1 " + Path.Combine(Utils.MONO_HOME, "bin/mono-service") + " -l:Worker.lock /home/ubuntu/backendservices/bin/WorkerService.exe --port 5000 --debug --no-deamon");
                workers_host_file.Add(it.Current.PrivateIpAddress + " 5000");

                while (it.MoveNext())
                {
                    IVirtualMachine vm = it.Current;

                    peers.Add("peer" + (count));
                    workpeers.Add("peer" + (count));
                    hosts.Add(vm.PrivateIpAddress + " peer" + count);
                    address.Add(vm.PublicIpAddress);
                    workers.Add("-n 1 " + Path.Combine(Utils.MONO_HOME, "bin/mono-service") + " -l:Worker" + count + ".lock /home/ubuntu/backendservices/bin/WorkerService.exe --port 5000 --debug --no-deamon");
                    workers_host_file.Add(vm.PrivateIpAddress + " 5000");
                    count++;
                }

                Utils.fileWrite(Utils.SCRIPTS, string.Format("peers.{0}", deploy_id), peers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workpeers.{0}", deploy_id), workpeers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("hosts.{0}", deploy_id), hosts.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("address.{0}", deploy_id), address.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workers.{0}", deploy_id), workers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workers_host_file.{0}", deploy_id), workers_host_file.ToArray());

                Console.WriteLine("generatePeerHosts exit: {0}", deploy_id);
            }
        }

        [WebMethod]
        public string release(string platform_address)
        {
            if (!running_instances.ContainsKey(platform_address))
                throw new Exception(string.Format("There is no running platform named {0} to be released", platform_address));

            IList<string> instanceIds = running_instances[platform_address].Item1;
            AmazonEC2Client client = running_instances[platform_address].Item2;

            InstancesManager.TerminateInstances(instanceIds, client);

            running_instances.Remove(platform_address);

           //deploy_id_queue.Enqueue(deploy_id_map[platform_address]);

            string return_message = string.Format("{0}  released !", platform_address);
            return return_message;
        }
    }
}
